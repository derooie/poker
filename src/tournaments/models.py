from django.db import models

from accounts.models import PokerUser


class TournamentDay(models.Model):
    date = models.DateField()
    location = models.CharField(max_length=64)

    def __str__(self):
        return str(self.date)


class TournamentRound(models.Model):
    day = models.ForeignKey(TournamentDay, on_delete=models.CASCADE, related_name='tournament_day')
    round_number = models.PositiveIntegerField()

    class Meta:
        unique_together = ('day', 'round_number',)

    def __str__(self):
        return str(self.round_number)





class TournamentPlayer(models.Model):
    tournament = models.ForeignKey(TournamentRound, on_delete=models.CASCADE, related_name='tournament')
    player = models.ForeignKey(PokerUser, on_delete=models.CASCADE)
    result = models.PositiveSmallIntegerField()
    price = models.PositiveSmallIntegerField(null=True, blank=True)

    # class Meta:
    #     unique_together = ('tournament', 'player',)


    def __str__(self):
        return self.player.username

from django.utils.datetime_safe import datetime
from django.views.generic import TemplateView, ListView, DetailView

from tournaments.models import TournamentRound, TournamentPlayer, TournamentDay


class IndexView(ListView):
    template_name = 'tournaments/index.html'
    model = TournamentDay


#
# class IndexView(TemplateView):
#     template_name = 'tournaments/index.html'
#
#     def get_context_data(self, **kwargs):
#         if self.request.user.is_authenticated:
#             print('User known')
#         else:
#             print('Anonymous')
#
#         # tournament = TournamentRound.objects.filter(round_number__date)
#
#         # Get poker round data by round (be aware this is a pk, not a round.
#         poker_round = TournamentRound.objects.get(day__date='2019-02-22', round_number=1)
#
#
#         print(poker_round.tournament)
#
#         # Fetch all users played in a round
#         for x in poker_round.tournament.all():
#             print(x)


class TournamentDayDetailsView(DetailView):
    template_name = 'tournaments/tournament_day_details.html'
    model = TournamentDay

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


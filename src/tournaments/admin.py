from django.contrib import admin

from tournaments.models import TournamentDay, TournamentRound, TournamentPlayer

admin.site.register(TournamentDay)
admin.site.register(TournamentRound)
admin.site.register(TournamentPlayer)

from django.urls import path, re_path
from . import views

app_name = 'tournaments'

urlpatterns = [

    path('', views.IndexView.as_view(), name='index'),
    path('<int:tournament_day>', views.TournamentDayDetailsView.as_view(), name='day-details'),
]


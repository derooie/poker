from django.db import models
from django.contrib.auth.models import AbstractUser


class PokerUser(AbstractUser):
    nick_name = models.CharField(max_length=64, blank=True, null=True)
    REQUIRED_FIELDS = ['email']

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import PokerUser


class PokerUserAdmin(UserAdmin):
    model = PokerUser

    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('nick_name',)}),
    )


admin.site.register(PokerUser, PokerUserAdmin)

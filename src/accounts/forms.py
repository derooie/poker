from django import forms

from accounts.models import PokerUser


class PersonalDetailsForm(forms.ModelForm):
    class Meta:
        model = PokerUser
        exclude = ('team', 'user',)

        # widgets = {
        #     'chart_type': forms.Select(choices=CHART_CHOICES),
        # }
